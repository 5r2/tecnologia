#+STARTUP: latexpreview
#+STARTUP: inlineimages
#+LATEX_HEADER: \usepackage[margin=1.0in]{geometry}
#+LaTeX_HEADER: \usepackage{pdfpages}

* Conclusiones
  
  Se cree, por la cercanía de los resultados con los de la norma, que al momento de la construcción,
  sí cumplía con la norma (se sabe que la norma fue siendo modificada con el pasar de los años, y la casa
  es vieja). Y se sabe también que los problemas graves como los del cableado, como la no cumplimentación a
  de los conectores, o los colores de los cables, son culpa de arreglos "caseros" a traves de los años
  por los inquilinos actuales.

  Sin lugar a dudas el domicilio no cumple con la normativa, y acondicionarlo para que la cumpla
  no es una tarea menor por la cantidad de faltas, pero sí se podrían arreglar las faltas más graves
  que suponen un riesgo, como el estado de las conexiones expuestas a la intemperie.
  
* Referencias
  - Norma AEA 90364 Seccion 770:
  
  https://www.buenosaires.gob.ar/sites/gcaba/files/90364-7-770-reimpresion_ene_2018_1.pdf
  
  - Guia AEA 770
  
  https://aea.org.ar/guia-aea-770/

  
#+ATTR_LATEX: :float sideways
[[./fotos/planoA.png]]
#+ATTR_LATEX: :float sideways
[[./fotos/planoB.png]]