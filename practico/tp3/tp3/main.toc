\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivos}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Actividad pr\IeC {\'a}ctica}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Medici\IeC {\'o}n de la variaci\IeC {\'o}n de la capacidad y la resistencia debidos a cambios de temperatura de operaci\IeC {\'o}n}{2}{section.1.3}
\contentsline {chapter}{\numberline {2}Desarrollo}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Mediciones y valores c\IeC {\'a}lculados}{4}{section.2.1}
\contentsline {subsection}{Capacitores}{4}{section*.4}
\contentsline {subsection}{Resistencias}{5}{section*.9}
\contentsline {section}{\numberline {2.2}Fotos de componentes}{6}{section.2.2}
\contentsline {chapter}{\numberline {3}Conclusiones}{10}{chapter.3}
