% Created 2019-04-23 Tue 08:49
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[margin=1.0in]{geometry}
\date{\today}
\title{Analisis FMECA de fuente}
\hypersetup{
 pdfauthor={},
 pdftitle={Analisis FMECA de fuente},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.1 (Org mode 9.1.9)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Modos de falla, efectos y analisis de criticidad}
\label{sec:orgcd1ef12}


\subsection{Failure Mode Effect Analysis}
\label{sec:orge68a84a}
\subsubsection{Hybrid device LM350 (LM350)}
\label{sec:orgd9dfa1e}
\begin{itemize}
\item \(\lambda_{\text{\{LM350\}}_{\text{abierto}}}\) = 601.491 \(\implies\) Ocurrencia=A; Severidad=I
\item \(\lambda_{\text{\{LM350\}}_{\text{degradado}}}\) = 306.642 \(\implies\) Ocurrencia=A; Severidad=II
\item \(\lambda_{\text{\{LM350\}}_{\text{corto}}}\) = 200.500 \(\implies\) Ocurrencia=A; Severidad=I
\item \(\lambda_{\text{\{LM350\}}_{\text{sinSalida}}}\) = 70.764 \(\implies\) Ocurrencia=A; Severidad=I
\end{itemize}

\subsubsection{1N5408 (D1,D2,D3,D4)}
\label{sec:orga44ec9e}
\begin{itemize}
\item \(\lambda_{\text{\{D(1-4)\}}_{\text{corto}}}\) = 0.221 \(\implies\) Ocurrencia=B; Severidad=II
\item \(\lambda_{\text{\{D(1-4)\}}_{\text{abierto}}}\) = 0.125 \(\implies\) Ocurrencia=C; Severidad=II
\item \(\lambda_{\text{\{D(1-4)\}}_{\text{cambioParams}}}\) = 0.087 \(\implies\) Ocurrencia=D; Severidad=IV
\end{itemize}

\subsubsection{1N4007 (D5,D6)}
\label{sec:orgb32607e}
\begin{itemize}
\item \(\lambda_{\text{\{D5,D6\}}_{\text{corto}}}\) = 0.007 \(\implies\) Ocurrencia=E; Severidad=I
\item \(\lambda_{\text{\{D5,D6\}}_{\text{abierto}}}\) = 0.003 \(\implies\) Ocurrencia=E; Severidad=IV
\item \(\lambda_{\text{\{D5,D6\}}_{\text{cambioParams}}}\) = 0.003 \(\implies\) Ocurrencia=E; Severidad=IV
\end{itemize}

\subsubsection{Capacitor Electrolitico 4700uF (C1)}
\label{sec:org919bd87}
\begin{itemize}
\item \(\lambda_{\text{\{C1\}}_{\text{corto}}}\) = 0.522 \(\implies\) Ocurrencia=B; Severidad=I
\item \(\lambda_{\text{\{C1\}}_{\text{abierto}}}\) = 0.344 \(\implies\) Ocurrencia=B; Severidad=II
\item \(\lambda_{\text{\{C1\}}_{\text{fuga}}}\) = 0.098 \(\implies\) Ocurrencia=C; Severidad=IV
\item \(\lambda_{\text{\{C1\}}_{\text{decCapacidad}}}\) = 0.020 \(\implies\) Ocurrencia=D; Severidad=III
\end{itemize}

\subsubsection{Capacitor Electrolitico 22uF (C2)}
\label{sec:orgaa845d4}
\begin{itemize}
\item \(\lambda_{\text{\{C2\}}_{\text{corto}}}\) = 0.199 \(\implies\) Ocurrencia=C; Severidad=I
\item \(\lambda_{\text{\{C2\}}_{\text{abierto}}}\) = 0.131 \(\implies\) Ocurrencia=C; Severidad=IV
\item \(\lambda_{\text{\{C2\}}_{\text{fuga}}}\) = 0.037 \(\implies\) Ocurrencia=D; Severidad=IV
\item \(\lambda_{\text{\{C2\}}_{\text{decCapacidad}}}\) = 0.007 \(\implies\) Ocurrencia=E; Severidad=IV
\end{itemize}

\subsubsection{Capacitor Electrolitico 100uF (C3)}
\label{sec:org00899f9}
\begin{itemize}
\item \(\lambda_{\text{\{C3\}}_{\text{corto}}}\) = 0.261 \(\implies\) Ocurrencia=B; Severidad=I
\item \(\lambda_{\text{\{C3\}}_{\text{abierto}}}\) = 0.172 \(\implies\) Ocurrencia=C; Severidad=II
\item \(\lambda_{\text{\{C3\}}_{\text{fuga}}}\) = 0.049 \(\implies\) Ocurrencia=D; Severidad=III
\item \(\lambda_{\text{\{C3\}}_{\text{decCapacidad}}}\) = 0.010 \(\implies\) Ocurrencia=D; Severidad=III
\end{itemize}

\subsubsection{Capacitor Ceramico (C4)}
\label{sec:org90d4147}
\begin{itemize}
\item \(\lambda_{\text{\{C4\}}_{\text{corto}}}\) = 0.031 \(\implies\) Ocurrencia=D; Severidad=I
\item \(\lambda_{\text{\{C4\}}_{\text{cambio}}}\) = 0.018 \(\implies\) Ocurrencia=D; Severidad=IV
\item \(\lambda_{\text{\{C4\}}_{\text{abierto}}}\) = 0.014 \(\implies\) Ocurrencia=D; Severidad=III
\end{itemize}

\subsubsection{Resistencia carbon (R1)}
\label{sec:org767f932}
\begin{itemize}
\item \(\lambda_{\text{\{R1\}}_{\text{cambio}}}\) = 0.002 \(\implies\) Ocurrencia=E; Severidad=IV
\item \(\lambda_{\text{\{R1\}}_{\text{abierto}}}\) = 0.001 \(\implies\) Ocurrencia=E; Severidad=I
\item \(\lambda_{\text{\{R1\}}_{\text{corto}}}\) = 0.0001 \(\implies\) Ocurrencia=E; Severidad=I
\end{itemize}

\subsubsection{Potenciometro (RV1)}
\label{sec:org34e9a74}
\begin{itemize}
\item \(\lambda_{\text{\{RV1\}}_{\text{abierto}}}\) = 0.070 \(\implies\) Ocurrencia=D; Severidad=I
\item \(\lambda_{\text{\{RV1\}}_{\text{erratico}}}\) = 0.053 \(\implies\) Ocurrencia=D; Severidad=II
\item \(\lambda_{\text{\{RV1\}}_{\text{corto}}}\) = 0.009 \(\implies\) Ocurrencia=E; Severidad=I
\end{itemize}

\begin{center}
\includegraphics[width=.9\linewidth]{hello-world.png}
\end{center}
\end{document}