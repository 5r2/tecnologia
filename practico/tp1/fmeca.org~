#+STARTUP: latexpreview
#+STARTUP: inlineimages
#+LATEX_HEADER: \usepackage[margin=1.0in]{geometry}

* Modos de falla, efectos y analisis de criticidad

** Failure Mode Effect Analysis
*** Hybrid device LM350 (LM350)
     - \lambda_{{LM350}_{abierto}} = 601.491 $\implies$ Ocurrencia=A; Severidad=I
     - \lambda_{{LM350}_{degradado}} = 306.642 $\implies$ Ocurrencia=A; Severidad=II
     - \lambda_{{LM350}_{corto}} = 200.500 $\implies$ Ocurrencia=A; Severidad=I
     - \lambda_{{LM350}_{sinSalida}} = 70.764 $\implies$ Ocurrencia=A; Severidad=I

*** 1N5408 (D1,D2,D3,D4)
     - \lambda_{{D(1-4)}_{corto}} = 0.221 $\implies$ Ocurrencia=B; Severidad=II
     - \lambda_{{D(1-4)}_{abierto}} = 0.125 $\implies$ Ocurrencia=C; Severidad=II
     - \lambda_{{D(1-4)}_{cambioParams}} = 0.087 $\implies$ Ocurrencia=D; Severidad=IV

*** 1N4007 (D5,D6)
     - \lambda_{{D5,D6}_{corto}} = 0.007 $\implies$ Ocurrencia=E; Severidad=I
     - \lambda_{{D5,D6}_{abierto}} = 0.003 $\implies$ Ocurrencia=E; Severidad=IV
     - \lambda_{{D5,D6}_{cambioParams}} = 0.003 $\implies$ Ocurrencia=E; Severidad=IV

*** Capacitor Electrolitico 4700uF (C1)
     - \lambda_{{C1}_{corto}} = 0.522 $\implies$ Ocurrencia=B; Severidad=I
     - \lambda_{{C1}_{abierto}} = 0.344 $\implies$ Ocurrencia=B; Severidad=II
     - \lambda_{{C1}_{fuga}} = 0.098 $\implies$ Ocurrencia=C; Severidad=IV
     - \lambda_{{C1}_{decCapacidad}} = 0.020 $\implies$ Ocurrencia=D; Severidad=III

*** Capacitor Electrolitico 22uF (C2)
     - \lambda_{{C2}_{corto}} = 0.199 $\implies$ Ocurrencia=C; Severidad=I
     - \lambda_{{C2}_{abierto}} = 0.131 $\implies$ Ocurrencia=C; Severidad=IV
     - \lambda_{{C2}_{fuga}} = 0.037 $\implies$ Ocurrencia=D; Severidad=IV
     - \lambda_{{C2}_{decCapacidad}} = 0.007 $\implies$ Ocurrencia=E; Severidad=IV

*** Capacitor Electrolitico 100uF (C3)
     - \lambda_{{C3}_{corto}} = 0.261 $\implies$ Ocurrencia=B; Severidad=I
     - \lambda_{{C3}_{abierto}} = 0.172 $\implies$ Ocurrencia=C; Severidad=II
     - \lambda_{{C3}_{fuga}} = 0.049 $\implies$ Ocurrencia=D; Severidad=III
     - \lambda_{{C3}_{decCapacidad}} = 0.010 $\implies$ Ocurrencia=D; Severidad=III

*** Capacitor Ceramico (C4)
     - \lambda_{{C4}_{corto}} = 0.031 $\implies$ Ocurrencia=D; Severidad=I
     - \lambda_{{C4}_{cambio}} = 0.018 $\implies$ Ocurrencia=D; Severidad=IV
     - \lambda_{{C4}_{abierto}} = 0.014 $\implies$ Ocurrencia=D; Severidad=III

*** Resistencia carbon (R1)
     - \lambda_{{R1}_{cambio}} = 0.002 $\implies$ Ocurrencia=E; Severidad=IV
     - \lambda_{{R1}_{abierto}} = 0.001 $\implies$ Ocurrencia=E; Severidad=I
     - \lambda_{{R1}_{corto}} = 0.0001 $\implies$ Ocurrencia=E; Severidad=I

*** Potenciometro (RV1)
     - \lambda_{{RV1}_{abierto}} = 0.070 $\implies$ Ocurrencia=D; Severidad=I
     - \lambda_{{RV1}_{erratico}} = 0.053 $\implies$ Ocurrencia=D; Severidad=II
     - \lambda_{{RV1}_{corto}} = 0.009 $\implies$ Ocurrencia=E; Severidad=I
       
** Matriz de criticidad

#+BEGIN_SRC ditaa :file hello-world.png
            /------------+-------------+------------+-------------\
            |            |             |            |             |
            |cBLU IV     |cBLU III     |cBLU II     |cBLU  I      |
            |            |             |            |             |
+-----------+------------+-------------+------------+-------------+
|cBLU       |cGRE        |cYEL         |cRED        |cRED         |
|           |            |             |            |             |
|     A     |            |             |   LM350    |    LM350    |
|           |            |             |            |             |
|           |            |             |            |             |
+-----------+------------+-------------+------------+-------------+
|cBLU       |cGRE        |cYEL         |cRED        |cRED         |
|           |            |             |            |             |
|     B     |            |             |   D(1-4)   |     C1      |
|           |            |             |     C1     |     C3      |
|           |            |             |            |             |
+-----------+------------+-------------+------------+-------------+
|cBLU       |cGRE        |cYEL         |cYEL        |cRED         |
|           |            |             |            |             |
|     C     |     C1     |             |   D(1-4)   |     C2      |
|           |     C2     |             |     C3     |             |
|           |            |             |            |             |
+-----------+------------+-------------+------------+-------------+
|cBLU       |cGRE        |cGRE         |cYEL        |cYEL         |
|           |            |             |            |             |
|           |   D(1-4)   |     C1      |            |             |
|     D     |     C2     |     C3      |    RV1     |     C4      |
|           |     C4     |     C4      |            |     RV1     |
|           |            |             |            |             |
|           |            |             |            |             |
+-----------+------------+-------------+------------+-------------+
|cBLU       |cGRE        |cGRE         |cGRE        |cGRE         |
|           |            |             |            |             |
|           |   D5,D6    |             |            |    D5,D6    |
|     E     |     C2     |             |            |     R1      |
|           |     R1     |             |            |     RV1     |
|           |            |             |            |             |
|           |            |             |            |             |
\-----------+------------+-------------+------------+-------------/
#+End_SRC
